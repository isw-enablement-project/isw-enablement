# Domain 용어 설명

## 1. Entity

## 2. Commands

## 3. Service

## 4. Event & Agent

---

# Entity

> Entity는 Properties로 구성된 개체로 솔루션 디자이너에서 Domain을 표현하는데 사용합니다.

## Properties

> Properties는 값 또는 범위에 대한 변수 및 필드갑

> | 이름             | 설명                                         |
> | ---------------- | -------------------------------------------- |
> | Text             | 형식이 없는 짧은 텍스트를 위한 필드          |
> | Text/E-mail      | MAIL 텍스트 필드                             |
> | Text/URL         | URL 텍스트 필드                              |
> | Integer          | 수소 자릿 수 없는 정수                       |
> | Boolean          | True 혹은 False속성                          |
> | Currency         | 수량 및 통화코드로 구성된 화폐 금액          |
> | Seletion Element | 특정값만 사용할수 있는 ENUM                  |
> | Date             | 시간 지정이 없는 날짜에 대한 유형            |
> | Geo point        | 위도와 경도가 있는 지리적 지점의 유형        |
> | Localized text   | Local과 이름으로 구성된 현재화 가능한 텍스트 |
> | Timestamp        | 날짜 및 시간이 포함된 유형                   |
> | Reference        | 다른 엔터티에 대한 링크                      |

## Entity 유형

1. Root Entity

   > 엔터티의 캡슐화된 클러스터에 대한 유일한 진입점입니다. 루트 엔터티에는 자체 수명 주기(생성/수명/삭제)가 있고 고유한 ID와 속성이 포함되어 있으며 데이터베이스 컬렉션에서 유지/삭제할 수 있습니다.

2. Entity

   > 루트 엔터티에 로컬 엔터티로 연결할 수 있는 속성을 포함하는 ID가 없는 단순한 값 엔터티입니다. 명령 및 서비스의 입력/출력과 이벤트에 대한 페이로드로도 사용할 수 있습니다. 이 유형의 엔터티는 지속되지 않습니다.

3. External Entity
   > 통합 네임스페이스 에서 통합되고 모델링된 외부 소스의 다른 엔터티와 매핑 및 상호 작용하는 데 사용됩니다 . 다른 도메인/시스템에 상주하는 엔터티에 대한 포인터를 나타내므로 원래 외부 엔터티의 모든 정의를 포함할 필요는 없으며 외부 엔터티의 로드를 통해 상주하는 위치에서 외부 엔터티를 로드하는 데 도움이 되는 일부 식별자 속성만 포함하면 됩니다 . 기능. 자체 속성을 포함할 수도 있습니다.

## Domain Example

![Servicing Order Diagram](images/servicing-order-domain.png)

---

# Command

> Commands는 Root Entity 라이프사이클에 포함되며 서비스에 비즈니스 로직으로 사용됩니다.Commands는 새 인스턴스를 만들거나 기존 인스턴스를 조작하는데 사용되며 오직 Root Entity만 Commands를 가질 수 있습니다.

## Command 유형

모든 커맨드는 Input값을 정의할 수 있으며 Output을 가지지 않습니다.
각각의 커맨드는 비즈니스 이벤트 및 오류를 추가할 수 있습니다.

1. Factory Commands

   > Root Entity의 새로운 인스턴스를(데이터베이스) 생성할때 사용하는 Commands

2. Instance Commands

   > 기존 인스턴스의(데이터베이스) 현재 상태를 조작하는데 사용합니다.

## Commands Example

![Servicing Order Diagram](images/servicing-order-domain.png)

---

# Service

---

# Event & Agent
